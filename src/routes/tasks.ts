import express from 'express';
import Docker from 'dockerode';

const router = express.Router();

router.get('/tasks/:service', async (req, res) => {
	const service = req.params.service;

	const docker = new Docker();

	const tasks = await docker.listTasks({ filters: { 'desired-state': ['running'], service: [`${service}`] } });
	const nodes = await docker.listNodes();
	const list = tasks.map((task) => {
		return {
			id: task.ID,
			internalIp: task.NetworksAttachments[0].Addresses[0].split('/')[0],
			publicIp: nodes.find((node) => node.ID === task.NodeID)?.Status.Addr,
		};
	});

	res.status(200).send(list);
});

export default router;
