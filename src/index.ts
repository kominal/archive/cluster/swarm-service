import express from 'express';
import cors from 'cors';
import { json } from 'body-parser';
import tasks from './routes/tasks';

const app = express();

app.use(cors());
app.use(
	json({
		limit: '16mb',
	})
);

app.use(tasks);

app.use((req, res) => {
	res.status(404).json({
		message: `Requested route (${req.url}) not found.`,
	});
});

app.listen(3000, () => {
	console.log(`Listening on port 3000`);
});

export default app;
